## What does this MR do and why?

_Describe in detail what your merge request does and why._

<!--
Please keep this description updated with any discussion that takes place so
that reviewers can understand your intent. Keeping the description updated is
especially important if they didn't participate in the discussion.
-->

## Screenshots or screen recordings

_Screenshots are required for UI changes, and strongly recommended for all other merge requests._

<!--
Please include any relevant screenshots or screen recordings that will assist
reviewers and future readers. If you need help visually verifying the change,
please leave a comment.
-->

## How to set up and validate locally

_Numbered steps to set up and validate the change are strongly suggested._

<!--
Example below:

1. Activate cloud_sql_proxy
   ```bash
   cloud_sql_proxy -instances=nuffsaid-staging:us-central1:staging14=tcp:5432 --enable_iam_login
   ```
2. Start web server
   ```bash
   flask run
   ```
3. Send a request to `http://localhost:5000/backend/graphql`
4. Review the response
-->

## MR acceptance checklist

This checklist encourages us to confirm any changes have been analyzed to reduce risks in quality, performance, reliability, security, and maintainability.

* [ ] I have evaluated the ticket NS-XXXX for this MR.
