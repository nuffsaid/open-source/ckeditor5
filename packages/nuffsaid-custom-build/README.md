# CKEditor 5 document editor build

[![npm version](https://badge.fury.io/js/%40ckeditor%2Fckeditor5-build-decoupled-document.svg)](https://www.npmjs.com/package/@ckeditor/ckeditor5-build-decoupled-document)
[![Coverage Status](https://coveralls.io/repos/github/ckeditor/ckeditor5/badge.svg?branch=master)](https://coveralls.io/github/ckeditor/ckeditor5?branch=master)
[![Build Status](https://travis-ci.com/ckeditor/ckeditor5.svg?branch=master)](https://app.travis-ci.com/github/ckeditor/ckeditor5)
![Dependency Status](https://img.shields.io/librariesio/release/npm/@ckeditor/ckeditor5-build-decoupled-document)

The document editor build for CKEditor 5, featuring the decoupled UI editor implementation. Read more about the [document editor build](https://ckeditor.com/docs/ckeditor5/latest/installation/getting-started/predefined-builds.html#document-editor) and see the [demo](https://ckeditor.com/docs/ckeditor5/latest/examples/builds/document-editor.html).

![CKEditor 5 decoupled document editor build screenshot](https://c.cksource.com/a/1/img/npm/ckeditor5-build-decoupled-document.png)

## Documentation

See:

-   [Installation](https://ckeditor.com/docs/ckeditor5/latest/installation/getting-started/quick-start.html) for how to install this package and what it contains.
-   [Basic API](https://ckeditor.com/docs/ckeditor5/latest/installation/getting-started/basic-api.html) for how to create an editor and interact with it.
-   [Configuration](https://ckeditor.com/docs/ckeditor5/latest/installation/getting-started/configuration.html) for how to configure the editor.
-   [Creating custom builds](https://ckeditor.com/docs/ckeditor5/latest/installation/getting-started/quick-start.html#building-the-editor-from-source) for how to customize the build (configure and rebuild the editor bundle).

## Quick start

First, install the build from npm:

```bash
npm install --save @nuffsaid/ckeditor5-nuffsaid
```

And use it in your JavaScript application:

```js
import DecoupledEditor from "@nuffsaid/ckeditor5-nuffsaid";

// Or using the CommonJS version:
// const DecoupledEditor = require( '@nuffsaid/ckeditor5-nuffsaid' );

DecoupledEditor.create(document.querySelector("#editor"))
	.then((editor) => {
		// The toolbar needs to be explicitly appended.
		document
			.querySelector("#toolbar-container")
			.appendChild(editor.ui.view.toolbar.element);

		window.editor = editor;
	})
	.catch((error) => {
		console.error("There was a problem initializing the editor.", error);
	});
```

**Note:** If you are planning to integrate CKEditor 5 deep into your application, it is actually more convenient and recommended to install and import the source modules directly (like it happens in `ckeditor.js`). Read more in the [Advanced setup guide](https://ckeditor.com/docs/ckeditor5/latest/builds/guides/integration/advanced-setup.html).

## Creating custom builds

A build is a simple [npm](https://www.npmjs.com) package (usually developed in a Git repository) with a predefined set of dependencies. Out of this repository, distribution files can be generated through the build process.

Some of the reasons for creating custom builds are:

-   Adding features which are not included in the existing builds, either from a third party or custom developed.
-   Removing unnecessary features present in a build.
-   Changing the [editor creator](https://ckeditor.com/docs/ckeditor5/latest/builds/guides/integration/basic-api.html#creating-an-editor).
-   Changing the [editor theme](https://ckeditor.com/docs/ckeditor5/latest/framework/guides/deep-dive/ui/theme-customization.html).
-   Changing the [localization language](https://ckeditor.com/docs/ckeditor5/latest/features/ui-language.html) of the editor.
-   Enabling bug fixes which are still not a part of any public release.

### Requirements

In order to start developing CKEditor 5 you will require:

-   [Node.js](https://nodejs.org/en/) 14.0.0+
-   npm 5.7.1+ (**note:** some npm 5+ versions were known to cause [problems](https://github.com/npm/npm/issues/16991), especially with deduplicating packages; upgrade npm when in doubt)
-   [Git](https://git-scm.com/)

### Getting started

Start cloning this repository:

```bash
git clone -b nuffsaid git@gitlab.com:nuffsaid/open-source/ckeditor5.git
```

Builds are available in the `packages/` directory. The directories are named `ckeditor5-build-*`.
For example, use the following command to get to the decoupled document build:

```bash
cd packages/ckeditor5-build-decoupled-document
```

> Alternatively, instead of creating a custom build you can [integrate CKEditor 5 directly from source](https://ckeditor.com/docs/ckeditor5/latest/builds/guides/integration/advanced-setup.html#scenario-2-building-from-source). This option allows for even more flexibility and requires less overhead (you will not need to fork the official build). However, it requires that you fully control the `webpack.config.js` file (which is not that easy in some environments &mdash; for example in [`angular-cli`](https://cli.angular.io/) or [`create-react-app`](https://github.com/facebook/create-react-app)).

> It is important that you use the `stable` branch of a build, not the `master` branch. The `master` branch might contain changes which are not yet compatible with the versions of CKEditor 5 source packages that were published on npm.

### Build anatomy

Every build contains the following files:

-   `build/ckeditor.js` &ndash; The ready-to-use editor bundle, containing the editor and all plugins.
-   `src/ckeditor.js` &ndash; The source entry point of the build. Based on it the `build/ckeditor.js` file is created by [webpack](https://webpack.js.org). It defines the editor creator, the list of plugins and the default configuration of a build.
-   `webpack-config.js` &ndash; webpack configuration used to build the editor.

### Customizing a build

In order to customize a build you need to:

-   Install missing dependencies.
-   Update the `src/ckeditor.js` file.
-   Update the build (the editor bundle in `build/`).

#### Installing dependencies

First, you need to install dependencies which are already specified in the build's `package.json`:

```bash
npm install
```

Then, you can add missing dependencies (i.e. packages you want to add to your build). The easiest way to do so is by typing:

```bash
npm install --save-dev <package-name>
```

This will install the package and add it to `package.json`. You can also edit `package.json` manually. Keep in mind however, that all packages (excluding `@ckeditor/ckeditor5-dev-*`) [must have the same version as the base editor package](https://ckeditor.com/docs/ckeditor5/latest/builds/guides/integration/installing-plugins.html#requirements).

> Due to the non-deterministic way how npm installs packages, it is recommended to run `rm -rf node_modules && npm install` when in doubt. This will prevent some packages from getting installed more than once in `node_modules/` (which might lead to broken builds).

> You can also give [Yarn](https://yarnpkg.com/lang/en/) a try.

#### Updating build configuration

If you added or removed dependencies, you will also need to modify the `src/ckeditor.js` file.

Every plugin that you want to include in the bundle should be added at this stage. You can also change the editor creator and specify the default editor configuration. For instance, your webpack entry file (`src/ckeditor.js`) may look like this:

```js
"use strict";

// The editor creator to use.
import ClassicEditorBase from "@ckeditor/ckeditor5-editor-classic/src/classiceditor";

import EssentialsPlugin from "@ckeditor/ckeditor5-essentials/src/essentials";
import AutoformatPlugin from "@ckeditor/ckeditor5-autoformat/src/autoformat";
import BoldPlugin from "@ckeditor/ckeditor5-basic-styles/src/bold";
import ItalicPlugin from "@ckeditor/ckeditor5-basic-styles/src/italic";
import HeadingPlugin from "@ckeditor/ckeditor5-heading/src/heading";
import LinkPlugin from "@ckeditor/ckeditor5-link/src/link";
import ListPlugin from "@ckeditor/ckeditor5-list/src/list";
import ParagraphPlugin from "@ckeditor/ckeditor5-paragraph/src/paragraph";

import CustomPlugin from "ckeditor5-custom-package/src/customplugin";
import OtherCustomPlugin from "../relative/path/to/some/othercustomplugin";

export default class ClassicEditor extends ClassicEditorBase {}

// Plugins to include in the build.
ClassicEditor.builtinPlugins = [
	EssentialsPlugin,
	AutoformatPlugin,
	BoldPlugin,
	ItalicPlugin,
	HeadingPlugin,
	LinkPlugin,
	ListPlugin,
	ParagraphPlugin,

	CustomPlugin,
	OtherCustomPlugin,
];

ClassicEditor.defaultConfig = {
	toolbar: ["heading", "|", "bold", "italic", "custombutton"],

	// This value must be kept in sync with the language defined in webpack.config.js.
	language: "en",
};
```

#### Rebuilding the bundle

After you changed the webpack entry file or updated some dependencies, it is time to rebuild the bundle. This will run a bundler (webpack) with a proper configuration (see `webpack.config.js`).

To do that, execute the following command:

```bash
npm run build
```

You can validate whether your new build works by opening the `sample/index.html` file in a browser (via HTTP, not as a local file). Make sure to **clear the cache**.

### Updating the build

You may decide to update your build at any time. Since it is a fork of the official build, you can simply merge the changes that happened meanwhile in that build, using Git commands:

```bash
git fetch origin
git merge origin/stable
```

You should handle eventual conflicts and verify the merged changes. After that, just follow the previous instructions for creating your build and test it.

> It is recommended to run `rm -rf node_modules && npm install` after you fetched changes from the upstream or updated versions of dependencies in `package.json` manually. This will prevent npm from installing packages more than once (which may lead to broken builds).

### Publishing your builds

After your merge request is approved and merged, GitLab CI/CD will build and publish your build. You can use the published build by adding an .npmrc file to the project that depends on the module. For example:

```text
@nuffsaid:registry=https://gitlab.com/api/v4/packages/npm/
```

Then, install the module:

```bash
npm install --save @nuffsaid/ckeditor5
```

## License

Licensed under the terms of [GNU General Public License Version 2 or later](http://www.gnu.org/licenses/gpl.html). For full details about the license, please check the `LICENSE.md` file or [https://ckeditor.com/legal/ckeditor-oss-license](https://ckeditor.com/legal/ckeditor-oss-license).
