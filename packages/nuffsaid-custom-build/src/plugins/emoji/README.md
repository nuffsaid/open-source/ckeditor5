# Emoji Plugin for CKEditor

This is a basic readme for the functionalities.

## How it works

This works very similar to the mentions plugin but focused on emojis. Key differences are:

1. No support for a custom marker. A colon `:` is the default marker.
2. We also default to `minChar` configuration of `2`.
3. You can provide a custom render for the emoji as part of the plugin configuration.
4. The rendered result can be different from the searched item. For example :fire: emoji rendered doesn't need to be the text `:fire:` but it can be the unicode value itself (or an image if it's a custom emoji) `🔥`.

## Configuration

All configuration is done with the `emoji` key when you are instantiating the editor.

```ts
interface Emoji {
  id: string;
  name: string;
  shortname: stringl;
  imageurl?: string;
  unicode?: string;
  skintone: number
}

interface RendererHelpers {
  createText: (text: string, attrs: Attributes) => Text;
  createImage: (attrs: Attributes) => EmptyElement;
}

interface EmojiConfig {
  feed?: Emoji[] | ((query: string) => Promise<Emoji[]> | Emoji[]);
  emojiContentRenderer?: (
    model: Emoji,
    writer: RendererHelpers,
  ) => Text | EmptyElement;
  onSelect?: (emoji: Emoji) => void;
  onToolbarClick?: (editor: DecoupledEditor, buttonRef: HTMLButtonElement) => void;
  itemRenderer?: (emoji: Emoji) => HTMLElement

}

// We also expose the `emoji` command
type EmojiExecute  = (args: {emoji: Emoji, range?: Selectable}) => void

```

### Feed

> When a feed is not provided, the plugin default to an empty array, so nothing will happen.

The feed could be an array or a function that returns an array of `Emoji`.

If an array is provided, we'll implement a simple filter that you can see on `src/emojihelper.js` with the function `createFeedCallback`.

The function provided can be async or not.

### Downcasting

We use the `emojiContentRenderer` for the `dataDowncast` and `editingDowncast` you **must** return a valid element using the functions provided on the second (`writer`) argument.

#### Default Downcast

The `defaultEmojiRenderer` creates checks if there's an `imageurl` and creates an image using the `name` as the alt text, or will default to use the `unicode` value and create a Text element.

### Rendering list items

You can provide an `itemRenderer` and return any valid DOM content, and it'll be used to render each item on the dropdown that opens when the user types the marker (`:`).

#### Default renderer

By default if the `emoji` has a `unicode` value it'll use the unicode and the short name, i.e., `🔥 :fire:`. If the emoji has an `imageurl` we'll create an image + name. If the emoji doesn't have unicode nor imageurl we just return the name.
