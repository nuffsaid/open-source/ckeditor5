import { Plugin } from 'ckeditor5/src/core';

import EmojiEditing from './emojiediting';
import EmojiUI from './emojiui';
import '../theme/ui.css';

export default class Emoji extends Plugin {
	static get requires() {
		return [EmojiEditing, EmojiUI];
	}

	init() {
		this._checkForMentionConflict();
	}

	_checkForMentionConflict() {
		const mentionConfig = this.editor.config.get('mention');

		if (mentionConfig && mentionConfig.feeds) {
			const mentionHasEmojiMarker = mentionConfig.feeds.some(
				(feed) => feed.marker.startsWith(':')
			);
			if (mentionHasEmojiMarker) {
				throw new Error(
					'The emoji plugin is using the `:` marker, this conflicts with the mention plugin and will cause issues.'
				);
			}
		}
	}
}
