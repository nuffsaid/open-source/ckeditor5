import { Plugin } from 'ckeditor5/src/core';
import {
	ButtonView,
	ContextualBalloon,
	clickOutsideHandler,
} from 'ckeditor5/src/ui';
import { Collection, keyCodes, Rect, logWarning } from 'ckeditor5/src/utils';
import { TextWatcher } from 'ckeditor5/src/typing';

import {
	checkIfStillInCompletionMode,
	getBalloonPanelPositions,
	createTestCallback,
	hasExistingEmoji,
	requestFeedText,
	createFeedCallback,
	defaultItemRenderer,
} from './emojihelper';
import EmojiListView from './ui/emojilistview';
import EmojiListItemView from './ui/emojilistitemview';
import DomWrapperView from './ui/domwrapperview';
import smiley from './ui/smiley.svg';

const defaultHandledKeyCodes = [
	keyCodes.arrowup,
	keyCodes.arrowdown,
	keyCodes.esc,
];

// Dropdown commit key codes.
const defaultCommitKeyCodes = [keyCodes.enter, keyCodes.tab];

export default class EmojiUI extends Plugin {
	static get requires() {
		return [ContextualBalloon];
	}

	get _isUIVisible() {
		return this._balloon.visibleView === this._emojiView;
	}

	constructor(editor) {
		super(editor);

		this._emojiView = this._createEmojiListView();
	}

	init() {
		const editor = this.editor;
		this._emojiConfig = editor.config.get('emoji') || {};
		this._emojiFeed = this._emojiConfig.feed;

		this._balloon = editor.plugins.get(ContextualBalloon);

		this._handleKeydown();

		clickOutsideHandler({
			emitter: this._emojiView,
			activator: () => this._isUIVisible,
			contextElements: [this._balloon.view.element],
			callback: () => this._hideUIAndRemoveMarker(),
		});

		this.feedCallback =
			typeof this._emojiFeed === 'function'
				? this._emojiFeed.bind(editor)
				: createFeedCallback(this._emojiFeed);
		this._setupWatcher();
		this.listenTo(editor, 'change:isReadOnly', () => {
			this._hideUIAndRemoveMarker();
		});
		this.on('requestFeed:response', (_, data) =>
			this._handleFeedResponse(data)
		);
		this.on('requestFeed:error', () => this._hideUIAndRemoveMarker());
		this._registerToolbar();
	}

	_registerToolbar() {
		const editor = this.editor;

		editor.ui.componentFactory.add('emoji', (locale) => {
			const view = new ButtonView(locale);

			view.set({
				label: editor.t('Add emoji'),
				icon: smiley,
				tooltip: true,
				class: 'custom-button add-emoji-button',
			});

			// Callback executed once the image is clicked.
			view.on('execute', () => {
				if (this._emojiConfig.onToolbarClick) {
					this._emojiConfig.onToolbarClick(this.editor, view.element);
				}
			});

			return view;
		});
	}

	_handleKeydown() {
		const editor = this.editor;
		const commitKeys = defaultCommitKeyCodes;
		const handledKeyCodes = defaultHandledKeyCodes.concat(commitKeys);
		editor.editing.view.document.on(
			'keydown',
			(evt, data) => {
				if (
					handledKeyCodes.includes(data.keyCode) &&
					this._isUIVisible
				) {
					data.preventDefault();
					evt.stop(); // Required for Enter key overriding.

					if (data.keyCode == keyCodes.arrowdown) {
						this._emojiView.selectNext();
					}

					if (data.keyCode == keyCodes.arrowup) {
						this._emojiView.selectPrevious();
					}

					if (commitKeys.includes(data.keyCode)) {
						this._emojiView.executeSelected();
					}

					if (data.keyCode == keyCodes.esc) {
						this._hideUIAndRemoveMarker();
					}
				}
			},
			{ priority: 'highest' }
		);
	}

	_createEmojiListView() {
		const locale = this.editor.locale;

		const emojiListView = new EmojiListView(locale);

		this._items = new Collection();

		emojiListView.items.bindTo(this._items).using((data) => {
			const { item, marker } = data;

			const { dropdownLimit = 10 } = this._emojiConfig;

			if (emojiListView.items.length >= dropdownLimit) {
				return;
			}

			const listItemView = new EmojiListItemView(locale);

			const view = this._renderItem(item);
			view.delegate('execute').to(listItemView);

			listItemView.children.add(view);
			listItemView.item = item;
			listItemView.marker = marker;

			listItemView.on('execute', () => {
				emojiListView.fire('execute', { item, marker });
			});

			return listItemView;
		});

		emojiListView.on('execute', (_, data) => {
			const editor = this.editor;
			const model = editor.model;

			const item = data.item;

			const marker = editor.model.markers.get('emoji');

			// Create a range on matched text.
			const end = model.createPositionAt(model.document.selection.focus);
			const start = model.createPositionAt(marker.getStart());
			const range = model.createRange(start, end);

			this._hideUIAndRemoveMarker();

			editor.execute('emoji', { emoji: item, range });

			editor.editing.view.focus();
		});

		return emojiListView;
	}

	_hideUIAndRemoveMarker() {
		if (this._balloon.hasView(this._emojiView)) {
			this._balloon.remove(this._emojiView);
		}

		if (checkIfStillInCompletionMode(this.editor)) {
			this.editor.model.change((writer) => writer.removeMarker('emoji'));
		}

		this._emojiView.position = undefined;
	}

	_renderItem(item) {
		const editor = this.editor;

		let view;
		let label = item.id;

		const renderer = this._emojiConfig.itemRenderer || defaultItemRenderer;

		const renderResult = renderer(item);

		if (typeof renderResult != 'string') {
			view = new DomWrapperView(editor.locale, renderResult);
		} else {
			label = renderResult;
		}

		if (!view) {
			const buttonView = new ButtonView(editor.locale);

			buttonView.label = label;
			buttonView.withText = true;

			view = buttonView;
		}

		return view;
	}

	_handleFeedResponse(data) {
		const { feed, marker } = data;

		if (!checkIfStillInCompletionMode(this.editor)) {
			return;
		}

		this._items.clear();

		for (const feedItem of feed) {
			const item =
				typeof feedItem != 'object'
					? { id: feedItem, text: feedItem }
					: feedItem;

			this._items.add({ item, marker });
		}

		const emojiMarker = this.editor.model.markers.get('emoji');

		if (this._items.length) {
			this._showOrUpdateUI(emojiMarker);
		} else {
			this._hideUIAndRemoveMarker();
		}
	}

	_showOrUpdateUI(marker) {
		if (this._isUIVisible) {
			this._balloon.updatePosition(
				this._getBalloonPanelPositionData(
					marker,
					this._emojiView.position
				)
			);
		} else {
			this._balloon.add({
				view: this._emojiView,
				position: this._getBalloonPanelPositionData(
					marker,
					this._emojiView.position
				),
				singleViewMode: true,
			});
		}

		this._emojiView.position = this._balloon.view.position;
		this._emojiView.selectFirst();
	}

	_getBalloonPanelPositionData(marker, preferredPosition) {
		const editor = this.editor;
		const editing = editor.editing;
		const domConverter = editing.view.domConverter;
		const mapper = editing.mapper;

		return {
			target: () => {
				let modelRange = marker.getRange();

				if (modelRange.start.root.rootName == '$graveyard') {
					modelRange =
						editor.model.document.selection.getFirstRange();
				}

				const viewRange = mapper.toViewRange(modelRange);
				const rangeRects = Rect.getDomRangeRects(
					domConverter.viewRangeToDom(viewRange)
				);

				return rangeRects.pop();
			},
			limiter: () => {
				const view = this.editor.editing.view;
				const viewDocument = view.document;
				const editableElement = viewDocument.selection.editableElement;

				if (editableElement) {
					return view.domConverter.mapViewToDom(editableElement.root);
				}

				return null;
			},
			positions: getBalloonPanelPositions(preferredPosition),
		};
	}

	_requestFeed(marker, feedText) {
		this._lastRequested = feedText;

		const feedResponse = this.feedCallback(feedText);

		const isAsynchronous = feedResponse instanceof Promise;

		if (!isAsynchronous) {
			this.fire('requestFeed:response', {
				feed: feedResponse,
				marker,
				feedText,
			});

			return;
		}

		feedResponse
			.then((response) => {
				if (this._lastRequested == feedText) {
					this.fire('requestFeed:response', {
						feed: response,
						marker,
						feedText,
					});
				} else {
					this.fire('requestFeed:discarded', {
						feed: response,
						marker,
						feedText,
					});
				}
			})
			.catch((error) => {
				this.fire('requestFeed:error', { error });
				logWarning('emoji-feed-callback-error', { marker });
			});
	}

	_setupWatcher() {
		const editor = this.editor;
		const marker = ':';
		const minimumCharacters = 2;
		const watcher = new TextWatcher(
			editor.model,
			createTestCallback(marker, minimumCharacters)
		);

		watcher.on('matched', (_, data) => {
			const selection = editor.model.document.selection;
			const focus = selection.focus;

			if (hasExistingEmoji(focus)) {
				this._hideUIAndRemoveMarker();
				return;
			}

			const feedText = requestFeedText(marker, data.text);
			const matchedTextLength = marker.length + feedText.length;

			// Create a marker range.
			const start = focus.getShiftedBy(-matchedTextLength);
			const end = focus.getShiftedBy(-feedText.length);

			const markerRange = editor.model.createRange(start, end);

			if (checkIfStillInCompletionMode(editor)) {
				const marker = editor.model.markers.get('emoji');

				editor.model.change((writer) => {
					writer.updateMarker(marker, { range: markerRange });
				});
			} else {
				editor.model.change((writer) => {
					writer.addMarker('emoji', {
						range: markerRange,
						usingOperation: false,
						affectsData: false,
					});
				});
			}

			this._requestFeed(marker, feedText);
		});

		watcher.on('unmatched', () => {
			this._hideUIAndRemoveMarker();
		});

		watcher.bind('isEnabled').to(editor.commands.get('emoji'));

		return watcher;
	}

	destroy() {
		super.destroy();

		// Destroy created UI components as they are not automatically destroyed (see ckeditor5#1341).
		this._emojiView.destroy();
	}
}
