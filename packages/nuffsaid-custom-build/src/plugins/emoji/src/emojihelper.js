import { env } from 'ckeditor5/src/utils';

export function createRegExp(marker, minimumCharacters) {
	const numberOfCharacters =
		minimumCharacters == 0 ? '*' : `{${minimumCharacters},}`;

	const openAfterCharacters = env.features.isRegExpUnicodePropertySupported
		? '\\p{Ps}\\p{Pi}"\''
		: '\\(\\[{"\'';
	const mentionCharacters = '\\S';

	// The pattern consists of 3 groups:
	// - 0 (non-capturing): Opening sequence - start of the line, space or an opening punctuation character like "(" or "\"",
	// - 1: The marker character,
	// - 2: Mention input (taking the minimal length into consideration to trigger the UI),
	//
	// The pattern matches up to the caret (end of string switch - $).
	//               (0:      opening sequence       )(1:  marker   )(2:                typed mention                 )$
	const pattern = `(?:^|[ ${openAfterCharacters}])([${marker}])([${mentionCharacters}]${numberOfCharacters})$`;

	return new RegExp(pattern, 'u');
}

export function createTestCallback(marker, minimumCharacters) {
	const regExp = createRegExp(marker, minimumCharacters);

	return (text) => regExp.test(text);
}

export function hasExistingEmoji(position) {
	// The text watcher listens only to changed range in selection - so the selection attributes are not yet available
	// and you cannot use selection.hasAttribute( 'mention' ) just yet.
	// See https://github.com/ckeditor/ckeditor5-engine/issues/1723.
	const hasMention =
		position.textNode && position.textNode.hasAttribute('name');

	const nodeBefore = position.nodeBefore;

	return (
		hasMention ||
		(nodeBefore &&
			nodeBefore.is('emoji') &&
			nodeBefore.hasAttribute('name'))
	);
}

export function requestFeedText(marker, text) {
	const regExp = createRegExp(marker, 0);

	const match = text.match(regExp);

	return match[2];
}

export function checkIfStillInCompletionMode(editor) {
	return editor.model.markers.has('emoji');
}

export function defaultEmojiRenderer(model, { createImage, createText }) {
	const unicode = model.getAttribute('unicode');
	const image = model.getAttribute('imageurl');
	const name = model.getAttribute('name');
	const shortname = model.getAttribute('shortname');

	return image
		? createImage({
				src: image,
				alt: name || shortname,
				class: 'emoji--image emoji',
				width: 14,
				height: 14,
				style: 'width: 1em; height: 1em; aspect-ratio: 1',
		  })
		: createText(unicode);
}

export function getAttributesFromEmojiElement(element) {
	return {
		unicode: element.getAttribute('data-unicode'),
		name: element.getAttribute('data-name'),
		shortname: element.getAttribute('data-shortname'),
		skintone: element.getAttribute('data-skintone'),
		imageurl: element.getAttribute('data-imageurl'),
	};
}

export function writerToContentRenderer(writer) {
	return {
		createText: (text, attrs) => writer.createText(text, attrs),
		createImage: (atrs) => writer.createEmptyElement('img', atrs),
	};
}

export function createEmojiView(model, viewWriter) {
	const emojiView = viewWriter.createContainerElement(
		'span',
		{
			class: 'emoji',
			'data-unicode': model.getAttribute('unicode'),
			'data-name': model.getAttribute('name'),
			'data-shortname': model.getAttribute('shortname'),
			'data-skintone': model.getAttribute('skintone'),
			'data-imageurl': model.getAttribute('imageurl'),
		},
		{ isAllowedInsideAttributeElement: true }
	);

	return emojiView;
}

export function getBalloonPanelPositions(preferredPosition) {
	const VERTICAL_SPACING = 3;
	const positions = {
		// Positions the panel to the southeast of the caret rectangle.
		caret_se: (targetRect) => {
			return {
				top: targetRect.bottom + VERTICAL_SPACING,
				left: targetRect.right,
				name: 'caret_se',
				config: {
					withArrow: false,
				},
			};
		},

		// Positions the panel to the northeast of the caret rectangle.
		caret_ne: (targetRect, balloonRect) => {
			return {
				top: targetRect.top - balloonRect.height - VERTICAL_SPACING,
				left: targetRect.right,
				name: 'caret_ne',
				config: {
					withArrow: false,
				},
			};
		},

		// Positions the panel to the southwest of the caret rectangle.
		caret_sw: (targetRect, balloonRect) => {
			return {
				top: targetRect.bottom + VERTICAL_SPACING,
				left: targetRect.right - balloonRect.width,
				name: 'caret_sw',
				config: {
					withArrow: false,
				},
			};
		},

		// Positions the panel to the northwest of the caret rect.
		caret_nw: (targetRect, balloonRect) => {
			return {
				top: targetRect.top - balloonRect.height - VERTICAL_SPACING,
				left: targetRect.right - balloonRect.width,
				name: 'caret_nw',
				config: {
					withArrow: false,
				},
			};
		},
	};

	// Returns only the last position if it was matched to prevent the panel from jumping after the first match.
	if (Object.prototype.hasOwnProperty.call(positions, preferredPosition)) {
		return [positions[preferredPosition]];
	}

	// By default return all position callbacks.
	return [
		positions.caret_se,
		positions.caret_sw,
		positions.caret_ne,
		positions.caret_nw,
	];
}

export const createFeedCallback = (emojiList) => {
	return (q) => {
		const query = q.toLowerCase();
		return emojiList
			.filter(
				(e) =>
					e.name.toLowerCase().includes(query) ||
					e.shortname.toLowerCase().includes(query)
			)
			.sort((a, b) => {
				const aShortName = a.shortname.toLowerCase();
				const bShortName = b.shortname.toLowerCase();

				if (aShortName.startsWith(`:${query}`)) return -1;
				if (bShortName.startsWith(`:${query}`)) return 1;

				const score =
					aShortName.indexOf(query) - bShortName.indexOf(query);
				const compare = aShortName.localeCompare(bShortName);
				// sort by match then sort by locale string
				return Math.min(score, compare);
			});
	};
};

export const defaultItemRenderer = (emoji) => {
	const container = document.createElement('div');
	let content = document.createElement('span');
	if (emoji.unicode) {
		content.appendChild(
			document.createTextNode(`${emoji.unicode} ${emoji.shortname}`)
		);
	} else if (emoji.imageurl) {
		const image = document.createElement('img');
		image.src = emoji.imageurl;
		image.alt = emoji.name;
		image.style.width = '12px';
		image.style.height = '12px';
		image.style.marginRight = '4px';
		const text = document.createTextNode(` - ${emoji.name}`);
		content.appendChild(image);
		content.appendChild(text);
	} else {
		container.append(document.createTextNode(emoji.shortname));
	}
	container.classList.add('emoji');
	container.appendChild(content);
	return container;
};
