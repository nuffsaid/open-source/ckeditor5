import { View } from 'ckeditor5/src/ui';

export default class DomWrapperView extends View {
	constructor(locale, domElement) {
		super(locale);
		this.template = false;
		this.domElement = domElement;
		this.domElement.classList.add('ck-button');
		this.set('isOn', false);

		this.on('change:isOn', (evt, name, isOn) => {
			if (isOn) {
				this.domElement.classList.add('highlighted');
			} else {
				this.domElement.classList.remove('highlighted');
			}
		});

		this.listenTo(this.domElement, 'click', () => {
			this.fire('execute');
		});
	}

	render() {
		super.render();

		this.element = this.domElement;
	}
}
