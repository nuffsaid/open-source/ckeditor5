import { ListItemView } from 'ckeditor5/src/ui';

export default class EmojiListItemView extends ListItemView {
	highlight() {
		const child = this.children.first;
		child.isOn = true;
	}

	removeHighlight() {
		const child = this.children.first;
		child.isOn = false;
	}
}
