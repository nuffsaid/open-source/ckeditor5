import { Plugin } from 'ckeditor5/src/core';
import { viewToModelPositionOutsideModelElement } from 'ckeditor5/src/widget';
import { Collection, logWarning } from 'ckeditor5/src/utils';

import {
	createEmojiView,
	defaultEmojiRenderer,
	getAttributesFromEmojiElement,
	writerToContentRenderer
} from './emojihelper';
import EmojiCommand from './emojicommand';

export default class EmojiEditing extends Plugin {
	init() {
		const editor = this.editor;
		this.emojiConfig = editor.config.get( 'emoji' ) || {};
		this._items = new Collection();

		editor.editing.mapper.on(
			'viewToModelPosition',
			viewToModelPositionOutsideModelElement(
				editor.model,
				viewElement => viewElement.hasClass( 'emoji' )
			)
		);

		if ( !this.emojiConfig.feed ) {
			logWarning( '[EmojiPlugin]: no feed provided.' );
		}

		editor.config.define( 'emoji', { feed: [] } );

		this._defineSchema();
		this._defineConverters();

		editor.commands.add( 'emoji', new EmojiCommand( editor ) );
	}

	_defineSchema() {
		const schema = this.editor.model.schema;

		schema.register( 'emoji', {
			allowWhere: '$text',
			allowContentOf: '$text',
			allowAttributesOf: '$text',
			inheritTypesFrom: '$text',
			allowAttributes: [
				'unicode',
				'skintone',
				'name',
				'shortname',
				'imageurl'
			]
		} );
	}

	_defineConverters() {
		const conversion = this.editor.conversion;
		const { emojiContentRenderer = defaultEmojiRenderer } =
			this.emojiConfig;

		conversion.for( 'upcast' ).elementToElement( {
			view: {
				name: 'span',
				classes: [ 'emoji' ],
				attributes: [ 'data-shortname' ]
			},
			model: ( viewElement, { writer: modelWriter } ) => {
				const emojiElement = viewElement.getChild( 0 ).parent;
				const attrs = getAttributesFromEmojiElement( emojiElement );
				return modelWriter.createElement( 'emoji', attrs );
			}
		} );

		conversion.for( 'editingDowncast' ).elementToElement( {
			model: 'emoji',
			view: ( model, { writer } ) => {
				const emojiView = createEmojiView( model, writer );
				const content = emojiContentRenderer(
					model,
					writerToContentRenderer( writer )
				);

				writer.insert( writer.createPositionAt( emojiView, 0 ), content );

				return emojiView;
			}
		} );

		conversion.for( 'dataDowncast' ).elementToElement( {
			model: 'emoji',
			view: ( model, { writer } ) => {
				const emojiView = createEmojiView( model, writer );
				const content = emojiContentRenderer(
					model,
					writerToContentRenderer( writer )
				);

				writer.insert( writer.createPositionAt( emojiView, 0 ), content );

				return emojiView;
			}
		} );
	}
}
