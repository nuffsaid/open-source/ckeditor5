import { Command } from 'ckeditor5/src/core';

export default class EmojiCommand extends Command {
	constructor(editor) {
		super(editor);
		this.handleOnSelect = (this.editor.config.get('emoji') || {}).onSelect;
	}

	execute({ emoji, range }) {
		const editor = this.editor;
		const model = editor.model;
		const selection = editor.model.document.selection;

		model.change((writer) => {
			const emojiContent = writer.createElement('emoji', {
				...Object.fromEntries(selection.getAttributes()),
				unicode: emoji.unicode,
				name: emoji.name,
				shortname: emoji.shortname || emoji.name,
				skintone: emoji.skintone || 1,
				imageurl: emoji.imageurl || '',
			});

			// ... and insert it into the document.
			model.insertContent(emojiContent, range);
			if (this.handleOnSelect) {
				this.handleOnSelect(emoji);
			}
		});
	}

	refresh() {
		const model = this.editor.model;
		const selection = model.document.selection;

		const isAllowed = model.schema.checkChild(selection.focus.parent, 'emoji');

		this.isEnabled = isAllowed;
	}
}
