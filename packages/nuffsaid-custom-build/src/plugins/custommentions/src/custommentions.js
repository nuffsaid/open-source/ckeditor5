import { Plugin } from 'ckeditor5/src/core';

export default class CustomMentions extends Plugin {
	init() {
		this._defineConverters();
	}

	_defineConverters() {
		const editor = this.editor;
		editor.conversion.for('upcast').elementToAttribute({
			view: {
				name: 'a',
				key: 'data-mention',
				classes: 'mention',
				attributes: {
					href: true,
					'data-user-id': true,
				},
			},
			model: {
				key: 'mention',
				value: (viewItem) => {
					const mentionAttribute = editor.plugins
						.get('Mention')
						.toMentionAttribute(viewItem, {
							// Add any other properties that you need.
							link: viewItem.getAttribute('href'),
							userId: viewItem.getAttribute('data-user-id'),
						});

					return mentionAttribute;
				},
			},
			converterPriority: 'high',
		});

		// Downcast the model 'mention' text attribute to a view <a> element.
		editor.conversion.for('downcast').attributeToElement({
			model: 'mention',
			view: (modelAttributeValue, { writer }) => {
				// Do not convert empty attributes (lack of value means no mention).
				if (!modelAttributeValue) {
					return;
				}

				const { onSelect } =
					(editor.config.get('custommentions') || []).find(
						(config) => config.marker === modelAttributeValue.id[0]
					) || {};
				if (onSelect) {
					onSelect(modelAttributeValue);
				}

				return writer.createAttributeElement(
					'a',
					modelAttributeValue.sid
						? {
								class: 'nuffsaid-mention nuffsaid-mention--slack',
								'data-mention': modelAttributeValue.id,
								sid: modelAttributeValue.sid,
						  }
						: {
								class: 'nuffsaid-mention',
								'data-mention': modelAttributeValue.id,
								href: `mailto:${modelAttributeValue.email}`,
						  },
					{
						priority: 20,
						id: modelAttributeValue.uid,
					}
				);
			},
			converterPriority: 'high',
		});
	}
}
