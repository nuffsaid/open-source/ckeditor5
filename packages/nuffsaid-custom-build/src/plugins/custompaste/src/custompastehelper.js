export const cleanEmptyHTML = (html) => {
	const result = html
		// clean up the empty tags
		.replaceAll(/<p>&nbsp;<\/p>/g, '')
		.replaceAll(/<p><\/p>/g, '')
		.replaceAll(/<span><\/span>/g, '')
		.replaceAll(/<p> <\/p>/g, '')
		.replaceAll(/<p><br \/><\/p>/g, '')
		.replaceAll(/<br \/><br \/>/g, '<br/>')
		// remove p inside li
		.replaceAll(/<li><p>/g, '<li>')
		.replaceAll(/<\/p><\/li>/g, '</li>')
		// remove empty tags
		.replaceAll(/<[^\/>][^>]*><\/[^>]+>/g, '')

	return result;
};
