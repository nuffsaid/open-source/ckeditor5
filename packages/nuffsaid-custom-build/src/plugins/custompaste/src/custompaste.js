import { Plugin } from 'ckeditor5/src/core';

import { cleanEmptyHTML } from './custompastehelper';

export default class CustomPaste extends Plugin {
	init() {
		const editor = this.editor;
		const config = editor.config.get('customPaste') || {};

		editor.plugins
			.get('ClipboardPipeline')
			.on('inputTransformation', (_, data) => {
				if (editor.isReadOnly) return;
				const dataTransfer = data.dataTransfer;
				const htmlContent = dataTransfer.getData('text/html');
				const { sanitizer } = config;
				// If no HTML or sanitizer was pasted, abort and let the clipboard feature handle the input.
				if (!htmlContent || !sanitizer) {
					return;
				}

				const safeHTML = cleanEmptyHTML(sanitizer(htmlContent));
				data.content = this.editor.data.htmlProcessor.toView(safeHTML);
			});
	}
}
