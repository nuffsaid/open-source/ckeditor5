import { SignatureButton as SignatureButtonDll, icons } from '../src';
import SignatureButton from '../src/signaturebutton';

import penNibRegular from './../theme/icons/pen-nib-regular.svg';

describe( 'CKEditor5 SignatureButton DLL', () => {
	it( 'exports SignatureButton', () => {
		expect( SignatureButtonDll ).to.equal( SignatureButton );
	} );

	describe( 'icons', () => {
		it( 'exports the "penNibRegular" icon', () => {
			expect( icons.penNibRegular ).to.equal( penNibRegular );
		} );
	} );
} );
