import penNibRegular from './../theme/icons/pen-nib-regular.svg';

export { default as SignatureButton } from './signaturebutton';

export const icons = {
	penNibRegular
};
