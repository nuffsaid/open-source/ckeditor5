import { Command, Plugin } from 'ckeditor5/src/core';
import { ButtonView } from 'ckeditor5/src/ui';

import penNibRegular from '../theme/icons/pen-nib-regular.svg';

const PLUGIN_BUTTON_LABEL = 'Signature';
const PLUGIN_COMMAND_NAME = 'signatureButton';
const PLUGIN_NAME = 'SignatureButton';

class SignatureButtonCommand extends Command {
	execute( callback ) {
		if ( typeof callback === 'function' ) {
			callback();
		}
	}
}

export default class SignatureButton extends Plugin {
	static get pluginName() {
		return PLUGIN_NAME;
	}

	init() {
		const editor = this.editor;
		const t = editor.t;

		editor.ui.componentFactory.add( PLUGIN_COMMAND_NAME, locale => {
			const buttonView = new ButtonView( locale );

			buttonView.set( {
				label: t( PLUGIN_BUTTON_LABEL ),
				icon: penNibRegular,
				tooltip: true,
				class: 'custom-button signature-button'
			} );

			this.listenTo( buttonView, 'execute', () => {
				editor.execute( PLUGIN_COMMAND_NAME, () => null );
			} );

			return buttonView;
		} );

		editor.commands.add(
			PLUGIN_COMMAND_NAME,
			new SignatureButtonCommand( editor )
		);
	}
}
