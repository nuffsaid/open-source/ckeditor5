import { AttachFilesButton as AttachFilesButtonDll, icons } from '../src';
import AttachFilesButton from '../src/attachfilesbutton';
import paperclipLight from './../theme/icons/paperclip-light.svg';

describe( 'CKEditor5 AttachFilesButton DLL', () => {
	it( 'exports AttachFilesButton', () => {
		expect( AttachFilesButtonDll ).to.equal( AttachFilesButton );
	} );

	describe( 'icons', () => {
		it( 'exports the "paperclipLight" icon', () => {
			expect( icons.paperclipLight ).to.equal( paperclipLight );
		} );
	} );
} );
