import ClassicEditor from '@ckeditor/ckeditor5-editor-classic/src/classiceditor';
import Essentials from '@ckeditor/ckeditor5-essentials/src/essentials';
import Heading from '@ckeditor/ckeditor5-heading/src/heading';
import Paragraph from '@ckeditor/ckeditor5-paragraph/src/paragraph';
import AttachFilesButton from '../src/attachfilesbutton';

/* global document */

describe( AttachFilesButton.name, () => {
	it( 'should be named', () => {
		expect( AttachFilesButton.pluginName ).to.equal( 'AttachFilesButton' );
	} );

	describe( 'init()', () => {
		let domElement, editor;

		beforeEach( async () => {
			domElement = document.createElement( 'div' );
			document.body.appendChild( domElement );

			editor = await ClassicEditor.create( domElement, {
				plugins: [
					Paragraph,
					Heading,
					Essentials,
					AttachFilesButton
				],
				toolbar: [
					'attachFilesButton'
				]
			} );
		} );

		afterEach( () => {
			domElement.remove();
			return editor.destroy();
		} );

		it( 'should load AttachFilesButton', () => {
			const attachFilesButton = editor.plugins.get( 'AttachFilesButton' );

			expect( attachFilesButton ).to.be.an.instanceof( AttachFilesButton );
		} );

		it( 'should add an icon to the toolbar', () => {
			expect( editor.ui.componentFactory.has( 'attachFilesButton' ) ).to.equal( true );
		} );

		it( 'should not add a text into the editor after clicking the icon', () => {
			const icon = editor.ui.componentFactory.create( 'attachFilesButton' );

			expect( editor.getData() ).to.equal( '' );

			icon.fire( 'execute' );

			expect( editor.getData() ).to.equal( '' );
		} );
	} );
} );
