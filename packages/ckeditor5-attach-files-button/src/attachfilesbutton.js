import { Command, Plugin } from 'ckeditor5/src/core';
import { ButtonView } from 'ckeditor5/src/ui';
import paperclipLightIcon from '../theme/icons/paperclip-light.svg';

const PLUGIN_BUTTON_LABEL = 'Attach files';
const PLUGIN_COMMAND_NAME = 'attachFilesButton';
const PLUGIN_NAME = 'AttachFilesButton';

class AttachFilesButtonCommand extends Command {
	execute( callback ) {
		if ( typeof callback === 'function' ) {
			callback();
		}
	}
}

export default class AttachFilesButtonPlugin extends Plugin {
	static get pluginName() {
		return PLUGIN_NAME;
	}

	init() {
		const editor = this.editor;
		const t = editor.t;

		editor.ui.componentFactory.add( PLUGIN_COMMAND_NAME, locale => {
			const buttonView = new ButtonView( locale );

			buttonView.set( {
				label: t( PLUGIN_BUTTON_LABEL ),
				icon: paperclipLightIcon,
				tooltip: true,
				class: 'custom-button attach-files-button'
			} );

			buttonView.on( 'execute', () => {
				editor.execute( PLUGIN_COMMAND_NAME, () => {} );

				editor.editing.view.focus();
			} );

			return buttonView;
		} );

		editor.commands.add( PLUGIN_COMMAND_NAME, new AttachFilesButtonCommand( editor ) );
	}
}
