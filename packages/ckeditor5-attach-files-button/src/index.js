import paperclipLight from './../theme/icons/paperclip-light.svg';

export { default as AttachFilesButton } from './attachfilesbutton';

export const icons = {
	paperclipLight
};
