import envelopeOpenTextRegular from './../theme/icons/envelope-open-text-regular.svg';

export { default as AddTemplateButton } from './addtemplatebutton';

export const icons = {
	envelopeOpenTextRegular
};
