import { Command, Plugin } from 'ckeditor5/src/core';
import { ButtonView } from 'ckeditor5/src/ui';

import envelopeOpenTextRegular from '../theme/icons/envelope-open-text-regular.svg';

const PLUGIN_BUTTON_LABEL = 'Add template';
const PLUGIN_COMMAND_NAME = 'addTemplateButton';
const PLUGIN_NAME = 'AddTemplateButton';

class AddTemplateButtonCommand extends Command {
	execute( callback ) {
		if ( typeof callback === 'function' ) {
			callback();
		}
	}
}

export default class AddTemplateButton extends Plugin {
	static get pluginName() {
		return PLUGIN_NAME;
	}

	init() {
		const editor = this.editor;
		const t = editor.t;

		editor.ui.componentFactory.add( PLUGIN_COMMAND_NAME, locale => {
			const buttonView = new ButtonView( locale );

			buttonView.set( {
				label: t( PLUGIN_BUTTON_LABEL ),
				icon: envelopeOpenTextRegular,
				tooltip: true,
				class: 'custom-button add-template-button'
			} );

			this.listenTo( buttonView, 'execute', () => {
				editor.execute( PLUGIN_COMMAND_NAME, () => null );
			} );

			return buttonView;
		} );

		editor.commands.add(
			PLUGIN_COMMAND_NAME,
			new AddTemplateButtonCommand( editor )
		);
	}
}
