import { AddTemplateButton as AddTemplateButtonDll, icons } from '../src';
import AddTemplateButton from '../src/addtemplatebutton';

import envelopeOpenTextRegular from './../theme/icons/envelope-open-text-regular.svg';

describe( 'CKEditor5 AddTemplateButton DLL', () => {
	it( 'exports AddTemplateButton', () => {
		expect( AddTemplateButtonDll ).to.equal( AddTemplateButton );
	} );

	describe( 'icons', () => {
		it( 'exports the "ckeditor" icon', () => {
			expect( icons.envelopeOpenTextRegular ).to.equal(
				envelopeOpenTextRegular
			);
		} );
	} );
} );
