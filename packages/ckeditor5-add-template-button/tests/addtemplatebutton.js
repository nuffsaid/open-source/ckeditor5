import Essentials from '@ckeditor/ckeditor5-essentials/src/essentials';
import Paragraph from '@ckeditor/ckeditor5-paragraph/src/paragraph';
import Heading from '@ckeditor/ckeditor5-heading/src/heading';
import ClassicEditor from '@ckeditor/ckeditor5-editor-classic/src/classiceditor';
import AddTemplateButton from '../src/addtemplatebutton';

/* global document */

describe( 'AddTemplateButton', () => {
	it( 'should be named', () => {
		expect( AddTemplateButton.pluginName ).to.equal( 'AddTemplateButton' );
	} );

	describe( 'init()', () => {
		let domElement, editor;

		beforeEach( async () => {
			domElement = document.createElement( 'div' );
			document.body.appendChild( domElement );

			editor = await ClassicEditor.create( domElement, {
				plugins: [ Paragraph, Heading, Essentials, AddTemplateButton ],
				toolbar: [ 'addTemplateButton' ]
			} );
		} );

		afterEach( () => {
			domElement.remove();
			return editor.destroy();
		} );

		it( 'should load AddTemplateButton', () => {
			const addTemplateButton = editor.plugins.get( 'AddTemplateButton' );

			expect( addTemplateButton ).to.be.an.instanceof( AddTemplateButton );
		} );

		it( 'should add an icon to the toolbar', () => {
			expect(
				editor.ui.componentFactory.has( 'addTemplateButton' )
			).to.equal( true );
		} );

		it( 'should add a text into the editor after clicking the icon', () => {
			const icon = editor.ui.componentFactory.create( 'addTemplateButton' );

			expect( editor.getData() ).to.equal( '' );

			icon.fire( 'execute' );

			expect( editor.getData() ).to.equal( '<p>Hello CKEditor 5!</p>' );
		} );
	} );
} );
